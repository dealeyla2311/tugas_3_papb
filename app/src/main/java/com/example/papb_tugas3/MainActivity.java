package com.example.papb_tugas3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    CheckBox cekRambut, cekAlis, cekKumis, cekJanggut;
    ImageView rambut, alis, kumis, janggut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       rambut = findViewById(R.id.imageView6);
       alis = findViewById(R.id.imageView5);
       kumis = findViewById(R.id.imageView8);
       janggut = findViewById(R.id.imageView3);
       cekRambut = findViewById(R.id.cekRambut);
       cekAlis = findViewById(R.id.cekAlis);
       cekKumis = findViewById(R.id.cekKumis);
       cekJanggut = findViewById(R.id.cekJanggut);

        //cek rambut
       cekRambut.setOnClickListener(new View.OnClickListener() {

           boolean visible;

           @Override
           public void onClick(View v) {
               visible = !visible;
               rambut.setVisibility(visible ? View.VISIBLE : View.GONE);
           }
       });

       //cek alis
        cekAlis.setOnClickListener(new View.OnClickListener() {

            boolean visible;

            @Override
            public void onClick(View v) {
                visible = !visible;
                alis.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });

        //cek kumis
        cekKumis.setOnClickListener(new View.OnClickListener() {

            boolean visible;

            @Override
            public void onClick(View v) {
                visible = !visible;
                kumis.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });

        //cek janggut
        cekJanggut.setOnClickListener(new View.OnClickListener() {

            boolean visible;

            @Override
            public void onClick(View v) {
                visible = !visible;
                janggut.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });

    }
}